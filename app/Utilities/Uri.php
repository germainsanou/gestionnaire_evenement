<?php 

namespace App\Utilities;

/**
 * return the uri of the current page
 */
class Uri
{
	

	public static function getUri()
	{
		$uri = trim($_SERVER['REQUEST_URI'], DIRECTORY_SEPARATOR);
		// dd($uri);
		return $uri;
	}
}