<?php 

namespace App\Utilities;

/**
 * 
 */
class EventHelpers
{
	
	public static function flash($message, $type = 'success')
	{
		session()->flash('notification.message', $message);
		session()->flash('notification.type', $type);
	}
}