<?php 

use App\Event;

if (!function_exists('format_price')) {
	function format_price(Event $event)
	{
		if ($event->isFree()) {
			return '<strong class="text-success">Gratuit !!!</strong>';
		}else{
			return sprintf('<span class="text-danger">%.2f FCFA</span>', $event->price);
		}
	}
}

if (!function_exists('format_date')) {
	function format_date($date)
	{
		return $date->format('d/m/Y H:i');
	}
}