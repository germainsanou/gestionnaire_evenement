<?php 
namespace App\Utilities;
use App\Url;
use Str;

/**
 * 
 */
class UrlHelpers
{
	
	public static function getUniqueShortened()
	{
		$random_shortened = Str::random(5);

		if (Url::whereShortened($random_shortened)->count() !=0) {
			return static::getUniqueShortened();
		}

		return $random_shortened;
	}
}