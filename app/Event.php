<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['title', 'body', 'city', 'price', 'start_at'];
    
   	protected $casts = [
   		'start_at' => 'datetime'
   	];


   	public function isFree()
   	{
   		return $this->price==0 ? true : false;
   	}

   	public function getRouteKeyName()
   	{
   		return 'slug';
   	}

      protected static function boot()
      {
         parent::boot();

         static::creating(function ($event)
         {
            $event->slug = \Str::slug($event->title);
         });
      }
}
