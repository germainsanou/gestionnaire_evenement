<?php

namespace App\Http\Requests\Events;

use Illuminate\Foundation\Http\FormRequest;

class CreateEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|min:3',
            'body' =>'required|min:12',
            'city' =>'required',
            'price' =>'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Le champ :attribute obligatoire',
            'min' => 'Il faut au moins :min caractere pour le champ :attribute ',
        ];
    }
}
