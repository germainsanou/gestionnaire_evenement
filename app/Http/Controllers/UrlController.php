<?php

namespace App\Http\Controllers;

use App\Utilities\UrlHelpers;
use App\Url;
use Illuminate\Http\Request;
use Validator;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bases-laravel/pages/shorturl/index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //VALIDATION
        $this->validate($request, ['url'=>'required|url']);
        $record = Url::firstOrCreate(
            ['url'=>$request->url],
            ['shortened'=>UrlHelpers::getUniqueShortened()]
        );

        return view('bases-laravel.pages.shorturl.shortened')->withShortened($record->shortened);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Url  $url
     * @return \Illuminate\Http\Response
     */
    public function show($shortened)
    {
        $trueUrl = Url::whereShortened($shortened)->firstOrFail();
        return redirect($trueUrl->url);
    }
}
