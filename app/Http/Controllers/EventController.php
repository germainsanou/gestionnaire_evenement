<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests\Events\CreateEvent;
use App\Utilities\EventHelpers;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('bases-laravel.pages.events.events')->withEvents(Event::Paginate(2));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bases-laravel/pages/events/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEvent $request)
    {   
        // $slug = \Str::slug($request->title);
        Event::create([
            'title' => $request->title,
            'body' => $request->body,
            'city' => $request->city,
            'price' => $request->price
        ]);
        
        // EventHelpers::flash('L\'evenement a ete creer avec succes !');
        \Flashy::message('L\'evenement a ete creer avec succes !');
        return redirect(route('events.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return view('bases-laravel.pages.events.event')->withEvent($event);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        return view('bases-laravel.pages.events.update')->withEvent($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(CreateEvent $request, Event $event)
    {

        $event->update($request->all());
        // EventHelpers::flash('L\'evenement a ete modifie avec succes !');
        \Flashy::message('L\'evenement a ete modifie avec succes !');
        return view('bases-laravel.pages.events.event')->withEvent($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();
        // EventHelpers::flash('L\'evenement a ete supprime avec succes !', 'danger');
        \Flashy::error('L\'evenement a ete suprime avec succes !');
        return redirect(route('events.index'));
    }
}
