<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('bases-laravel/pages/home');
    }

    public function about()
    {
        return view('bases-laravel/pages/about');
    }

    public function help()
    {
        return view('bases-laravel/pages/help');
    }
}
