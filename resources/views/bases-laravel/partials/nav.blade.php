<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto p-2">
      <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}"><span class="logo {{ ($uri::getUri()=='home'||$uri::getUri()=='') ? 'active' : '' }}">HOME</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('about')}}"><span class="onglet {{ $uri::getUri()=='about' ? 'active' : '' }}">About</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('help')}}"><span class="onglet {{ $uri::getUri()=='help' ? 'active' : '' }}">Help</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('events.index')}}"><span class="onglet {{ $uri::getUri()=='events' ? 'active' : '' }}">Events</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('urls.index')}}"><span class="onglet {{ $uri::getUri()=='urls' ? 'active' : '' }}">Short Url</span></a>
      </li>
    </ul>
    <form class="form-inline mt-2 mt-md-0">
      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>