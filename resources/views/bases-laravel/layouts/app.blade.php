@inject('uri','App\Utilities\Uri')

<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  {{-- fichiers de style externe --}}
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/style.css') }}">

  <title>demo - {{ $title ?? $title }}</title>
</head>
<body>
  @include('bases-laravel.partials.nav')

  <main class="container main">

    {{-- @if (session()->get('notification.message'))
      <div class="alert alert-{{ session('notification.type') }} alert-dismissible fade show" role="alert">
        {{ session()->get('notification.message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif --}}

    @yield('content')

  </main>

  

  @include('bases-laravel.partials.footer')
  <div class="test"></div>
  

  {{-- mes script externes --}}
  <script src="{{ asset('/js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('/js/events.js') }}"></script>
  @include('flashy::message')
</body>
</html>
