@extends('bases-laravel.layouts.app', ['title' => 'shorturl'])

@section('content')
	<div class="row">
		<div class="col-md-6 mx-auto p-3">
			<h1 class="text-center lead text-secondary">Amazing url shortner there !!!</h1>
			<div>
				<form action="{{ route('urls.store') }}" method="POST">
					@csrf
					<input type="text" name="url" class="form-control form-control-sm mb-2 border-secondary" placeholder="Enter your original url" value="{{ old('url') }}">
					{!! $errors->first('url', '<p class="errors">:message</p>') !!}
					<input type="submit" value="GET SHORT URL" class="btn btn-sm btn-primary btn-block">
				</form>
			</div>
		</div>
	</div>
@endsection