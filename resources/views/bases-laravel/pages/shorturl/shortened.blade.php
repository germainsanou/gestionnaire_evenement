@extends('bases-laravel.layouts.app', ['title' => 'shortened'])

@section('content')
	<div class="row">
		<div class="col-md-8 mx-auto text-center mt-5">
			<h2 class="lead text-secondary text-center">YOUR SHORT URL BELOW !</h2>
			<a href="{{ route('urls.show', $shortened) }}" target="_blank">
				{{ env('APP_URL')}}/urls/{{ $shortened }}
			</a>
		</div>
	</div>
@endsection