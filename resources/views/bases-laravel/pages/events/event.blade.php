@extends('bases-laravel.layouts.app', ['title' => 'events'])

@section('content')
  <h1 class="lead">{{ $event->title }}</h1>
  <p class="text-justify">{{ $event->body }}</p>
  <a href="{{ route('events.edit', $event) }}" class="btn btn-sm btn-outline-secondary">EDITER</a>
  <a href="{{ route('events.destroy', $event) }}" class="btn btn-sm btn-outline-danger" data-method="DELETE" data-confirm="êtes vous sûr?">DELETE</a>
@endsection
