<?php $event = new App\Event(); ?>
@extends('bases-laravel/layouts/app',['title' => 'create-event'])

@section('content')
	<div class="row">
		<div class="col-md-8 mx-auto">
			<h1 class="text-center lead text-success">Creer un evenement</h1>
			<form action="{{ route('events.store') }}" method="POST">
				@csrf
		  	@include('bases-laravel/pages/events/_form', ['submitButtonText' => 'Creer un evenement'])
			</form>
		</div>
	</div>
@endsection