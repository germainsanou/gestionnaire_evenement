@extends('bases-laravel.layouts.app', ['title' => $event->title])

@section('content')
	<div class="col">
		<div class="col-md-8 mx-auto">
		  <h1 class="lead">Update event</h1>
		  <form action="{{ route('events.update', $event)}}" method="POST">
		  	@method('PUT')
		  	@csrf
		  	@include('bases-laravel/pages/events/_form', ['submitButtonText' => 'Mettre a jour l\'evenement'])
		  </form>
		</div>
	</div>
@endsection
