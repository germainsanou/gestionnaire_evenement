<div class="form-group">
	<label for="title">Title: </label>
	<input type="text" class="form-control {{ $errors->has('title')?'is-invalid':'' }}" id="title" name="title" value="{{ old('title')??$event->title }}">
	{!! $errors->first('title', '<span class="invalid-feedback">:message</span>') !!}
</div>

<div class="form-group">
	<label for="body">Text content</label>
	<textarea name="body" id="body" cols="30" rows="6" class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}">
		{{ old('body') ?? $event->body }}
	</textarea>
	{!! $errors->first('body', '<span class="invalid-feedback">:message</span>') !!}
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
	  	<label for="city">City</label>
	  	<input type="text" name="city" id="city" class="form-control {{ $errors->has('city') ? 'is-invalid' :''}}" value="{{ old('city') ?? $event->city }}">
	  	{!! $errors->first('city', '<span class="invalid-feedback">:message<span>') !!}
  	</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
	  	<label for="price">Price</label>
	  	<input type="text" value="{{ old('price') ?? $event->price }}" name="price" id="price" class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}">
	  	{!! $errors->first('price', '<span class="invalid-feedback">:message</span>') !!}
  	</div>
	</div>
</div>
<input type="submit" class="btn btn-primary btn-block" value="{{ $submitButtonText }}">