@extends('bases-laravel.layouts.app', ['title' => 'events'])

@section('content')
  <div class="row">
  	<div class="col-md-8 mx-auto">
	  	<div class="mb-3">
	  		<span class="text-secondary mr-3">Events count: {{ $events->count() }}</span>
	  		<a href="{{ route('events.create') }}" class="btn btn-sm btn-outline-success">Creer un Evenement</a>
	  	</div>

	  	@if ($events->count() > 0 )
			<div class="row">
		    @foreach($events as $event)
					<div class="col-md-6">
						<article class="pb-5">
							<h2 class="lead text-info">{{ $event->title }}</h2>
							<p class="text-justify">{{ $event->body }}</p>
							date: {{ format_date($event->start_at) }} *** price: {!! format_price($event) !!} <br>
							<a href="{{ route('events.show',$event) }}" class="btn btn-sm btn-primary">Detail</a>
						</article>
					</div>
		    @endforeach
			</div>
		    {{-- {{ $events->links('vendor/pagination/simple-default') }} --}}
		    {{ $events->links() }}
	    @else
			<p class="text-secondary text-center">Aucun evenement</p>
	  	@endif
  	</div>
  </div>
@endsection
